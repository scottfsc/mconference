package felipe.saraiva.com.mconference.view;

import android.accounts.NetworkErrorException;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import felipe.saraiva.com.mconference.R;
import felipe.saraiva.com.mconference.adapters.PinnedExpandableAdapter;
import felipe.saraiva.com.mconference.adapters.SubSessionListAdapter;
import felipe.saraiva.com.mconference.application.TintableImageView;
import felipe.saraiva.com.mconference.work.ConferenceController;
import felipe.saraiva.com.mconference.work.NotifyService;
import felipe.saraiva.com.mconference.work.Utils;
import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by felip on 25/10/2015.
 */
public class ConferenceActivity extends Activity implements TabHost.OnTabChangeListener, SearchView.OnQueryTextListener,
        RadioGroup.OnCheckedChangeListener {

    private static final String tagSched = "sched", tagNews = "news", tagGall = "gallery", tagLoc = "loc";
    private static String TAG = "ui.ConferenceActivity";
    private ParseGeoPoint geoPoint;
    private ConferenceController controller;
    private ParseObject session = null;
    private ActionBar mActionBar;
    private TabHost tabHost;
    private ViewPager galleryPager, locationPager;
    private PagerAdapter galleryPAdapter = null, locationAdapter = null;
    private ViewFlipper scheduleFlipper, galleryFlipper;
    private SearchView mSearchView;
    private SwipeRefreshLayout refreshGallery;
    private SegmentedGroup segmentedGroup;
    private GridView imageGrid = null;
    private ListView reminderNone, reminderAll, subsessionListView, newsListView;
    private BaseAdapter newsAdapter = null;
    private BaseAdapter galleryAdapter = null;
    private PinnedExpandableAdapter sessions = null, favorites = null;
    private FloatingGroupExpandableListView sessionsList;
    private ParseObject conferenceObject;
    private ParseObject userConference;
    private RadioButton all, fav;
    private ImageView deleteConference;
    private View conferenceButton;

    private View actionBarMain, actionBarSession, actionBarReminder;

    private boolean isFav;
    private String currentTag;
    private ProgressBar pBar;
    private TextView messageTextView;
    private boolean isNewsUpdating = true;
    private boolean isGalleryUpdating = true;
    private boolean isSchedUpdating = true;
    private boolean isLocationUpdating = true;

    private ImageView reminder;

    private TextView sessionTitle, sessionInfo, addToFav;
    private int aux1;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        controller = new ConferenceController(this);
        if(Utils.conference == null){
            setResult(RESULT_OK);
            finish();
            return;
        }

        conferenceObject = Utils.conference;
        isFav = getIntent().getBooleanExtra("isfav", false);
        geoPoint = conferenceObject.getParseGeoPoint("geoLocation");
        setContentView(R.layout.activity_conference);

        init();

        onTabChanged(tagSched);
        AdView adView = (AdView) findViewById(R.id.adView);
        if (this.conferenceObject.getInt("level") == 0) {
            adView.loadAd(new AdRequest.Builder().addTestDevice("72E9C15D64DA56D3E4091F89648B0427").build());
        } else {
            adView.setVisibility(View.GONE);
        }

    }

    private void getUserConference(){
        try {
            ParseQuery<ParseObject> parseQuery = new ParseQuery<>("UserConference");
            parseQuery.fromPin("userConference");
            parseQuery.whereEqualTo("conference", conferenceObject);
            parseQuery.whereEqualTo("user", ParseUser.getCurrentUser());
            parseQuery.include("favoriteSessions");
            userConference = parseQuery.getFirst();
        } catch (ParseException e){
            userConference = null;
        }
    }

    private void init() {
        configureActionBar();
        mSearchView = (SearchView) findViewById(R.id.searchView);
        segmentedGroup = (SegmentedGroup) findViewById(R.id.Segmented);
        locationPager = (ViewPager) findViewById(R.id.locationPager);
        refreshGallery = (SwipeRefreshLayout) findViewById(R.id.refreshGallery);
        galleryPager = (ViewPager) findViewById(R.id.imageGallery);
        imageGrid = (GridView) findViewById(R.id.imageGrid);
        scheduleFlipper = (ViewFlipper) findViewById(R.id.conferenceSchedule);
        galleryFlipper = (ViewFlipper) findViewById(R.id.conferenceGallery);
        reminderNone = (ListView) findViewById(R.id.reminderNone);
        reminderAll = (ListView) findViewById(R.id.reminderTimes);
        newsListView = (ListView) findViewById(R.id.conferenceNews);
        subsessionListView = (ListView) findViewById(R.id.sessionDesc_Subsession);
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        pBar = (ProgressBar) findViewById(R.id.pBar);
        messageTextView = (TextView) findViewById(R.id.mainMessage);
        sessionsList = (FloatingGroupExpandableListView) findViewById(R.id.floatingExpandableList);
        all = (RadioButton) findViewById(R.id.SegmentedAll);
        fav = (RadioButton) findViewById(R.id.SegmentedFav);
        conferenceButton = findViewById(R.id.my_add_button);
        if (isFav) {
            conferenceButton.setVisibility(View.GONE);
        } else {
            deleteConference.setVisibility(View.INVISIBLE);
        }
        sessionTitle = (TextView) findViewById(R.id.sessionTitle);
        sessionInfo = (TextView) findViewById(R.id.sessionInfo);
        addToFav = (TextView) findViewById(R.id.addToFav);
        reminderNone.setAdapter(getRemindersAdapter(false));
        reminderNone.setOnItemClickListener(getAlarmItemClicked(false));
        reminderAll.setOnItemClickListener(getAlarmItemClicked(true));
        reminderAll.setAdapter(getRemindersAdapter(true));
        imageGrid.setOnItemClickListener(galleryItemClicked());
        segmentedGroup.setOnCheckedChangeListener(this);
        mSearchView.setOnQueryTextListener(this);
        sessionsList.setOnChildClickListener(sessionClick());
        addToFav.setOnClickListener(favoritarSessao());
        refreshGallery.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPhotos0();
            }
        });
        setupTabs();

    }

    private void configureActionBar() {
        mActionBar = getActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowCustomEnabled(true);
            actionBarMain = getLayoutInflater().inflate(R.layout.actionbarhomelayout, null);
            ((ImageView) actionBarMain.findViewById(R.id.actionbarLeft)).setImageResource(R.drawable.ic_close_white_36dp);
            ((TextView) actionBarMain.findViewById(R.id.actionbarText)).setText(conferenceObject.getString("shortName"));
            deleteConference = (ImageView) actionBarMain.findViewById(R.id.actionbarRight);
            deleteConference.setImageResource(R.drawable.ic_delete_white_24dp);
            actionBarSession = getLayoutInflater().inflate(R.layout.actionbarhomelayout, null);
            ((ImageView) actionBarSession.findViewById(R.id.actionbarLeft)).setImageResource(R.drawable.ic_chevron_left_white_36dp);
            ((TextView) actionBarSession.findViewById(R.id.actionbarText)).setText(conferenceObject.getString("shortName"));
            reminder = (ImageView) actionBarSession.findViewById(R.id.actionbarRight);
            reminder.setImageResource(R.drawable.ic_reminder);
            setRightListener(actionBarSession, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nextSchedPage();
                }
            });
            reminder.setActivated(false);
            actionBarReminder = getLayoutInflater().inflate(R.layout.actionbarhomelayout, null);
            ((ImageView) actionBarReminder.findViewById(R.id.actionbarLeft)).setImageResource(R.drawable.ic_chevron_left_white_36dp);
            ((TextView) actionBarReminder.findViewById(R.id.actionbarText)).setText(R.string.reminder);
            setActionBar(0);
        }
    }

    private void showToast() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ConferenceActivity.this, R.string.no_network_toast, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setRightListener(View v, View.OnClickListener listener) {
        v.findViewById(R.id.actionbarRight).setOnClickListener(listener);
    }

    private void setActionBar(int help) {
        switch (help) {
            default:
            case 0:
                mActionBar.setCustomView(actionBarMain, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                break;
            case 1:
                mActionBar.setCustomView(actionBarSession, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                break;
            case 2:
                mActionBar.setCustomView(actionBarReminder, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                break;
        }
    }

    private void setupTabs() {
        tabHost.setup();
        addTab(tagSched, R.string.schedule, R.drawable.ic_event_available_white_24dp, R.id.conferenceSchedule);
        addTab(tagNews, R.string.news, R.drawable.ic_library_books_white_24dp, R.id.News);
        addTab(tagGall, R.string.gallery, R.drawable.ic_photo_library_white_24dp, R.id.conferenceGallery);
        addTab(tagLoc, R.string.location, R.drawable.ic_map_white_24dp, R.id.conferenceLocation);
        tabHost.setOnTabChangedListener(this);
    }

    private void addTab(String tag, @StringRes int title, @DrawableRes int indicator, @IdRes int resourceId) {
        View v = getLayoutInflater().inflate(R.layout.tabitemmodel, null);
        TintableImageView image = (TintableImageView) v.findViewById(R.id.tabImage);
        image.setImageResource(indicator);
        image.setColorFilter(getResources().getColorStateList(R.color.image_tint));
        TextView textView = (TextView) v.findViewById(R.id.tabTitle);
        textView.setTextColor(getResources().getColorStateList(R.color.text_tint));
        textView.setText(title);
        tabHost.addTab(tabHost.newTabSpec(tag).setIndicator(v).setContent(resourceId));
    }

    private void loadSession() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                isSchedUpdating = true;
                showProgressBar(true);
                sessions = controller.getSessions();
                reloadFavs();
                if(sessions.getGroupCount() > 0)
                if (segmentedGroup.getCheckedRadioButtonId() == all.getId())
                    updateListView(sessionsList, sessions, R.string.no_sess, tagSched);
                else
                    updateListView(sessionsList, favorites, R.string.no_fav_session, tagSched);
                loadSession0();
            }
        }).start();
    }

    private void loadSession0() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    sessions = controller.getSessionsOnline();
                    reloadFavs();
                } catch (NetworkErrorException e) {
                    showToast();
                }
                if (segmentedGroup.getCheckedRadioButtonId() == all.getId())
                    updateListView(sessionsList, sessions, R.string.no_sess, tagSched);
                else
                    updateListView(sessionsList, favorites, R.string.no_fav_session, tagSched);
                isSchedUpdating = false;
            }
        }).start();
    }

    private void reloadFavs() {
        favorites = controller.getFavorites(userConference);
        if (segmentedGroup.getCheckedRadioButtonId() != all.getId()) {
            updateListView(sessionsList, favorites, R.string.no_fav_session, tagSched);
        }
    }

    private void loadNews() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                isNewsUpdating = true;
                showProgressBar(true);
                newsAdapter = controller.getNews();
                if(newsAdapter.getCount() > 0)
                updateListView(newsListView, newsAdapter, R.string.no_news, tagNews);
                loadNews0();
            }
        }).start();
    }

    private void loadNews0() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    newsAdapter = controller.getNewsOnline();
                } catch (NetworkErrorException e) {
                    showToast();
                }
                updateListView(newsListView, newsAdapter, R.string.no_news, tagNews);
                isNewsUpdating = false;
            }
        }).start();
    }

    private void loadPhotos() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        float x = dm.widthPixels / dm.xdpi;
        float y = dm.heightPixels / dm.ydpi;
        double diagonal = Math.sqrt(x*x + y*y);
        Log.i("Device Screen is: ", "" + diagonal + "inches.");
        if (diagonal > 5.9)
            imageGrid.setNumColumns(4);
        else
            imageGrid.setNumColumns(3);
        new Thread(new Runnable() {
            @Override
            public void run() {
                isGalleryUpdating = true;
                showProgressBar(true);
                galleryAdapter = controller.getPhotos();
                galleryPAdapter = controller.getGalleryPageAdapter();
                if(galleryAdapter.getCount() > 0)
                updateListView(imageGrid, galleryAdapter, R.string.no_photos, tagGall);
                setGalleryPager();
                loadPhotos0();
            }
        }).start();
    }

    private void loadPhotos0() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    galleryAdapter = controller.getPhotosOnline();
                    galleryPAdapter = controller.getGalleryPageAdapter();
                } catch (NetworkErrorException e) {
                    showToast();
                }
                updateListView(imageGrid, galleryAdapter, R.string.no_photos, tagGall);
                updateListView(galleryPager, galleryPAdapter, R.string.no_photos, tagGall);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (refreshGallery.isRefreshing())
                            refreshGallery.setRefreshing(false);
                    }
                });
                isGalleryUpdating = false;
            }
        }).start();
    }

    private void setGalleryPager() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                galleryPager.setAdapter(galleryPAdapter);
            }
        });
    }

    private void loadLocation() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                isLocationUpdating = true;
                showProgressBar(true);
                locationAdapter = controller.getLocationPhotos();
                if(locationAdapter.getCount() > 0)
                updateListView(locationPager, locationAdapter, R.string.no_loc, tagLoc);
                loadLocation0();
            }
        }).start();
    }

    private void loadLocation0() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    locationAdapter = controller.getLocationPhotosOnline();
                } catch (NetworkErrorException e) {
                    showToast();
                }
                updateListView(locationPager, locationAdapter, R.string.no_loc, tagLoc);
                isLocationUpdating = false;
            }
        }).start();
    }

    private void updateListView(final FloatingGroupExpandableListView view, final PinnedExpandableAdapter adapter,
                                @StringRes final int message, final String tag) {
        showProgressBar(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (adapter != null) {
                    WrapperExpandableListAdapter adapter0 = new WrapperExpandableListAdapter(adapter);
                    view.setAdapter(adapter0);
                    for (int i = 0; i < view.getExpandableListAdapter().getGroupCount(); i++) {
                        Date d = ((ParseObject) view.getExpandableListAdapter().getChild(i, 0)).getDate("startDate");
                        GregorianCalendar gc = new GregorianCalendar(), gc1 = new GregorianCalendar();
                        gc1.setTime(d);
                        if (gc.get(GregorianCalendar.YEAR) < gc1.get(GregorianCalendar.YEAR)
                                || gc.get(GregorianCalendar.MONTH) < gc1.get(GregorianCalendar.MONTH)
                                || (gc.get(GregorianCalendar.MONTH) == gc1.get(GregorianCalendar.MONTH)
                                && gc.get(GregorianCalendar.DAY_OF_MONTH) <= gc1.get(GregorianCalendar.DAY_OF_MONTH)))
                            view.expandGroup(i);
                    }
                }
                if (adapter == null || adapter.getGroupCount() == 0) {
                    if (currentTag.equals(tag) && scheduleFlipper.getDisplayedChild() == 0)
                        showMessage(message);
                }
            }
        });
    }

    private void nextSchedPage() {
        scheduleFlipper.setInAnimation(this, R.anim.slide_in_from_right);
        scheduleFlipper.setOutAnimation(this, R.anim.slide_out_to_left);
        switch (scheduleFlipper.getDisplayedChild()) {
            default:
            case 0:
                setActionBar(1);
                aux1 = 1;
                scheduleFlipper.showNext();
                break;
            case 1:
                setActionBar(2);
                aux1 = 2;
                scheduleFlipper.showNext();
        }
    }

    private void nextGalleryPage() {
        galleryFlipper.setInAnimation(this, R.anim.slide_in_from_right);
        galleryFlipper.setOutAnimation(this, R.anim.slide_out_to_left);
        ((ImageView) actionBarMain.findViewById(R.id.actionbarLeft)).setImageResource(R.drawable.ic_chevron_left_white_36dp);
        galleryFlipper.showNext();
    }

    private void previousSchedPage() {
        scheduleFlipper.setInAnimation(this, R.anim.slide_in_from_left);
        scheduleFlipper.setOutAnimation(this, R.anim.slide_out_to_right);
        switch (scheduleFlipper.getDisplayedChild()) {
            default:
            case 1:
                setActionBar(0);
                aux1 = 0;
                scheduleFlipper.showPrevious();
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (segmentedGroup.getCheckedRadioButtonId() != all.getId() && favorites.getGroupCount() == 0)
                            showMessage(R.string.no_fav_session);
                    }
                }, 500);
                break;
            case 2:
                setActionBar(1);
                aux1 = 1;
                scheduleFlipper.showPrevious();
        }
    }


    private void previousGalleryPage() {
        galleryFlipper.setInAnimation(this, R.anim.slide_in_from_left);
        galleryFlipper.setOutAnimation(this, R.anim.slide_out_to_right);
        if (galleryFlipper.getDisplayedChild() > 0) {
            ((ImageView) actionBarMain.findViewById(R.id.actionbarLeft)).setImageResource(R.drawable.ic_close_white_36dp);
            galleryFlipper.showPrevious();
        }
    }

    private void configSessionPage(ParseObject session) {
        this.session = session;
        boolean favSess = false;
        ArrayList<Object> l = new ArrayList<>();
        String s = session.getString("descriptionText");
        if (s != null && s.length() > 0)
            l.add(s);
        if (userConference != null) {
            favSess = userConference.getList("favoriteSessions") != null && userConference.getList("favoriteSessions").contains(session);
        }
        sessionTitle.setText(session.getString("title"));
        StringBuilder std = new StringBuilder();
        String location = session.getString("location");
        if (location == null || location.equals("null") || location.equals(""))
            location = getString(R.string.not_espec);
        std.append(getString(R.string.where)).append(" ").append(location)
                .append('\n').append(getString(R.string.bein)).append(" ")
                .append(Utils.getFullDate(session.getDate("startDate")))
                .append('\n').append(getString(R.string.ending)).append(" ")
                .append(Utils.getFullDate(session.getDate("endDate")));
        sessionInfo.setText(std);
        addToFav.setText(getString(favSess ? R.string.remove_from : R.string.add_to_sess));
        List sub = session.getList("subsessions");
        if (sub != null)
            l.addAll(sub);
        reminder.setActivated(Utils.alarms.containsKey("ObjectId"));
        subsessionListView.setAdapter(new SubSessionListAdapter(this, l));
    }

    private void updateListView(final AdapterView view, final BaseAdapter adapter, @StringRes final int message, final String tag) {
        showProgressBar(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setAdapter(adapter);
                if (adapter == null || adapter.getCount() == 0) {
                    if (currentTag.equals(tag))
                        showMessage(message);
                }
            }
        });
    }

    private void updateListView(final ViewPager view, final PagerAdapter adapter, @StringRes final int message, final String tag) {
        showProgressBar(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setAdapter(adapter);
                if (adapter == null || adapter.getCount() == 0) {
                    Log.d(TAG, "updateListView() returned: " + currentTag + " -- " + tag);
                    Log.d(TAG, "updateListView() returned: " + getString(message));
                    if (currentTag.equals(tag))
                        showMessage(message);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (tagSched.equals(currentTag)) {
            if (scheduleFlipper.getDisplayedChild() > 0) {
                previousSchedPage();
                return;
            }
        } else {
            if (tagGall.equals(currentTag)) {
                if (galleryFlipper.getDisplayedChild() != 0) {
                    previousGalleryPage();
                    return;
                }
            }
        }
        View v = mActionBar.getCustomView();
        if (v.equals(actionBarMain))
            setResult(isFav ? 3 : RESULT_OK);
        finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        View v = this.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (v != null)
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if ("".equals(newText)) {
            if (segmentedGroup.getCheckedRadioButtonId() == all.getId()) {
                updateListView(sessionsList, sessions, R.string.no_sess, tagSched);
            } else
                updateListView(sessionsList, favorites, R.string.no_fav_session, tagSched);
        } else {
            PinnedExpandableAdapter adapter = controller.filtrarSessoes(newText, segmentedGroup.getCheckedRadioButtonId() != all.getId());
            updateListView(sessionsList, adapter, R.string.no_sess_found, tagSched);
        }
        return true;
    }

    @Override
    public void onTabChanged(String tabId) {
        hideMessage();
        showProgressBar(false);
        Log.d(TAG, "onTabChanged() called with: " + "tabId = [" + tabId + "]");
        currentTag = tabId;
        deleteConference.setVisibility(View.INVISIBLE);
        deleteConference.setOnClickListener(null);
        setActionBar(0);
        previousGalleryPage();
        if (tabId.equals(tagNews)) {
            if (newsAdapter == null) {
                loadNews();
            } else {
                if (isNewsUpdating)
                    showProgressBar(true);
                else if (newsAdapter.getCount() == 0) {
                    showProgressBar(false);
                    showMessage(R.string.no_news);
                }
            }

        } else if (tabId.equals(tagGall)) {
            if (galleryAdapter == null) {
                loadPhotos();
            } else {
                if (isGalleryUpdating)
                    showProgressBar(true);
                else if (galleryAdapter.getCount() == 0) {
                    showProgressBar(false);
                    showMessage(R.string.no_photos);
                }
            }

        } else if (tabId.equals(tagLoc)) {
            if (locationAdapter == null) {
                loadLocation();
            } else {
                if (isLocationUpdating)
                    showProgressBar(true);
                else if (locationAdapter.getCount() == 0) {
                    showProgressBar(false);
                    showMessage(R.string.no_loc);
                }
            }
        } else {
            setActionBar(aux1);
            if (isFav && !deleteConference.isShown()) {
                deleteConference.setVisibility(View.VISIBLE);
                deleteConference.setOnClickListener(getDeleter());
            }
            if (sessions == null)
                loadSession();
            else
                onCheckedChanged(segmentedGroup, segmentedGroup.getCheckedRadioButtonId());
        }
        Log.d(TAG, "onTabChanged() returned: " + currentTag);
    }

    private View.OnClickListener getDeleter() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(ConferenceActivity.this)
                        .setTitle(R.string.delete_conf)
                        .setMessage(R.string.delete_message)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                conferenceButton.setClickable(false);
                                favoritar(false);
                                deleteConference.setVisibility(View.INVISIBLE);
                                deleteConference.setOnClickListener(null);
                                conferenceButton.setClickable(true);
                            }
                        })
                        .setNegativeButton(R.string.no, null).show();
            }
        };
    }

    private void showProgressBar(final boolean visible) {
        hideMessage();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (visible) {
                    Log.d(TAG, "showProgressBar() returned: " + true);
                    pBar.setVisibility(View.VISIBLE);
                } else {
                    Log.d(TAG, "showProgressBar() returned: " + false);
                    pBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showMessage(@StringRes final int message) {
        showProgressBar(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageTextView.setText(message);
                messageTextView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageTextView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (isSchedUpdating) {
            hideMessage();
            showProgressBar(true);
        } else {
            if (checkedId == all.getId()) {
                updateListView(sessionsList, sessions, R.string.no_sess, tagSched);
            } else {
                updateListView(sessionsList, favorites, R.string.no_fav_session, tagSched);
            }
        }
    }

    private void favoritar(boolean favoritar) {
        if (favoritar) {
            if (userConference == null) {
                conferenceObject.pinInBackground("favorites");
                userConference = new ParseObject("UserConference");
                userConference.put("conference", conferenceObject);
                userConference.put("user", ParseUser.getCurrentUser());
                userConference.saveEventually();
                userConference.pinInBackground("userConference");
                ParseInstallation.getCurrentInstallation().addUnique("FavoritesConferences", conferenceObject.getObjectId());
                ParseInstallation.getCurrentInstallation().saveEventually();
                Utils.favoriteConferences.add(conferenceObject);
                isFav = true;
                setResult(3);
            }
        } else {
            if (userConference != null) {
                isFav = false;
                Utils.favoriteConferences.remove(conferenceObject);
                ParseInstallation.getCurrentInstallation().removeAll("FavoritesConferences", Collections.singletonList(conferenceObject.getObjectId()));
                ParseInstallation.getCurrentInstallation().saveEventually();
                userConference.deleteEventually();
                conferenceObject.unpinInBackground();
                conferenceButton.setVisibility(View.VISIBLE);
                Log.d(TAG, "favoritar() returned: " + userConference);
                userConference = null;
                setResult(3);
            }
        }
        reloadFavs();

    }

    public ExpandableListView.OnChildClickListener sessionClick() {
        return new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ParseObject p = (ParseObject) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);
                configSessionPage(p);
                nextSchedPage();
                return true;
            }
        };
    }

    public ArrayAdapter<String> getRemindersAdapter(boolean all_none) {
        if (all_none) {
            return new ArrayAdapter<>(this, R.layout.textview_reminders, R.id.textReminder,
                    new String[]{getString(R.string.timeOfEvent)
                            , getString(R.string.fiveminbef)
                            , getString(R.string.fiftminbef)
                            , getString(R.string.thirtyminbef)
                            , getString(R.string.onehrbef)
                            , getString(R.string.twohrbef)
                            , getString(R.string.onedbef)});
        } else {
            return new ArrayAdapter<>(this, R.layout.textview_reminders, R.id.textReminder, new String[]{getString(R.string.none)});
        }
    }

    public AdapterView.OnItemClickListener getAlarmItemClicked(boolean all_none) {
        if (all_none)
            return new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    long when = 0L;
                    AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(ConferenceActivity.this, NotifyService.class);
                    intent.putExtra("message", position);
                    intent.putExtra("title", session.getString("title"));
                    intent.putExtra("ObjectId", session.getObjectId());
                    switch (position) {
                        default:
                            break;
                        case 1:
                            when = 300000L;
                            break;
                        case 2:
                            when = 900000L;
                            break;
                        case 3:
                            when = 1800000L;
                            break;
                        case 4:
                            when = 3600000L;
                            break;
                        case 5:
                            when = 7200000L;
                            break;
                        case 6:
                            when = 86400000L;
                    }
                    if (new Date(session.getDate("startDate").getTime() - when).before(new Date())) {
                        Toast.makeText(ConferenceActivity.this, R.string.cant_set_alarm, Toast.LENGTH_SHORT).show();
                    } else {
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(ConferenceActivity.this, (int) System.currentTimeMillis(), intent, 0);
                        Utils.alarms.put(session.getObjectId(), pendingIntent);
                        manager.set(AlarmManager.RTC_WAKEUP, session.getDate("startDate").getTime() - when, pendingIntent);
                        reminder.setActivated(true);
                        addToFav.callOnClick();
                    }
                    onBackPressed();
                }
            };
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
                manager.cancel(Utils.alarms.get(session.getObjectId()));
                Utils.alarms.remove(session.getObjectId());
                reminder.setActivated(false);
                onBackPressed();
            }
        };
    }

    private AdapterView.OnItemClickListener galleryItemClicked() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                galleryPager.setCurrentItem(position);
                nextGalleryPage();
            }
        };
    }

    public void eventPlace(View view) {
        if (geoPoint != null) {
            Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?daddr=" + geoPoint.getLatitude() + "," + geoPoint.getLongitude());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(mapIntent);
            }
        }
    }

    public void add_to_fav(View view) {
        deleteConference.setClickable(false);
        favoritar(true);
        view.setVisibility(View.GONE);
        deleteConference.setVisibility(View.VISIBLE);
        deleteConference.setOnClickListener(getDeleter());
        deleteConference.setClickable(true);
    }

    private View.OnClickListener favoritarSessao() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userConference == null || !controller.isFav(session)) {
                    if (userConference == null)
                        favoritar(true);
                    addToFav.setText(R.string.remove_from);
                    deleteConference.setVisibility(View.VISIBLE);
                    deleteConference.setOnClickListener(getDeleter());
                    conferenceButton.setVisibility(View.GONE);
                    userConference.addAllUnique("favoriteSessions", Collections.singletonList(session));
                    userConference.saveEventually();
                } else {
                    addToFav.setText(R.string.add_to_sess);
                    userConference.removeAll("favoriteSessions", Collections.singletonList(session));
                    userConference.saveEventually();
                }
                reloadFavs();
            }
        };
    }

    public void close(View view) {
        onBackPressed();
    }
}
