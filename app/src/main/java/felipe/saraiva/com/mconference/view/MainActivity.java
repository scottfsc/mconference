package felipe.saraiva.com.mconference.view;

import android.accounts.NetworkErrorException;
import android.app.ActionBar;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseUser;

import java.util.HashMap;

import felipe.saraiva.com.mconference.R;
import felipe.saraiva.com.mconference.adapters.ConferenceListAdapter;
import felipe.saraiva.com.mconference.application.TintableImageView;
import felipe.saraiva.com.mconference.work.MainController;
import felipe.saraiva.com.mconference.work.Utils;

/**
 * Created by felip on 25/10/2015.
 */
public class MainActivity extends Activity implements TabHost.OnTabChangeListener, SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener, ListView.OnScrollListener {

    private static String TAG = "ui.MainActivity";
    private static int RESULT_TRUE = 3;
    private String tagFav = "favs", tagSearch = "search";
    private SwipeRefreshLayout refreshLayout;
    private TabHost tabHost;
    private ListView favorites, search;
    private SearchView mSearchView;
    private ActionBar mActionBar;
    private ConferenceListAdapter favoritesAdapter;
    private ProgressBar pBar;
    private TextView messageTextView;
    private RelativeLayout mainTela;
    private String currentTag = tagFav;
    private View actionBarText;
    private boolean updating = false;
    private Button mainButton;
    private MainController controller;

    private void configParse(){
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("GCMSenderId", "434689507272");
        installation.saveInBackground();
        Log.d("mApplication","installation: "+installation.getObjectId());

        ParsePush.subscribeInBackground("global");
        ParseUser.enableAutomaticUser();
        ParseUser.getCurrentUser().increment("RunCount");
        ParseUser.getCurrentUser().saveInBackground();
        ParseACL defaultACL = new ParseACL(ParseUser.getCurrentUser());System.out.println("Is user autenticated ?" +ParseUser.getCurrentUser().isAuthenticated());
        ParseACL.setDefaultACL(defaultACL, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configParse();
        try{
            Utils.alarms = (HashMap<String, PendingIntent>) savedInstanceState.getSerializable("pendingIntens");
            if(Utils.alarms == null)
                Utils.alarms = new HashMap<>();
        } catch (Exception e){
            Utils.alarms = new HashMap<>();
        }
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_main);
        controller = new MainController(this);
        init();
//        AdView adView = (AdView) findViewById(R.id.adView1);
//        adView.loadAd(new AdRequest.Builder().addTestDevice("72E9C15D64DA56D3E4091F89648B0427").build());
        loadFavorites(true);
    }

    private void init(){
        configureActionBar();
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        favorites = (ListView) findViewById(R.id.listFavs);
        search = (ListView) findViewById(R.id.listaSearch);
        pBar = (ProgressBar) findViewById(R.id.pBar);
        messageTextView = (TextView) findViewById(R.id.mainMessage);
        setupTabs();
        loadFavorites(true);
        mainButton = (Button) findViewById(R.id.mainButton);
        DisplayMetrics a = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(a);
        refreshLayout.setOnRefreshListener(this);
        mainTela = (RelativeLayout) findViewById(R.id.mainTela);
        favorites.setOnItemClickListener(getConferenceClickListener());
        favorites.setOnScrollListener(this);
        search.setOnScrollListener(this);
        search.setOnItemClickListener(getConferenceClickListener());
    }

    private void configureActionBar(){
        mActionBar = getActionBar();
        Log.d(TAG, "configureActionBar() returned: " + mActionBar);
        if(mActionBar!= null){
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowCustomEnabled(true);
            mSearchView = new SearchView(this);
            mSearchView.setIconifiedByDefault(false);
            mSearchView.setQueryHint(getString(R.string.search));
            actionBarText = getLayoutInflater().inflate(R.layout.actionbarmain, null);
            setActionBar(true);
            mSearchView.setOnQueryTextListener(this);
            int id = mSearchView.getContext()
                    .getResources()
                    .getIdentifier("android:id/search_src_text", null, null);
            int id2 = mSearchView.getContext()
                    .getResources()
                    .getIdentifier("android:id/search_button", null, null);
            ImageView imageView = (ImageView) mSearchView.findViewById(id2);
            imageView.setImageResource(R.drawable.ic_search_white_24dp);
            TextView textView = (TextView) mSearchView.findViewById(id);
            textView.setTextColor(Color.WHITE);
            textView.setHintTextColor(Color.LTGRAY);
        }
    }

    private void setActionBar(boolean first){
        if(first){
            mActionBar.setCustomView(actionBarText , new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        } else {
            mActionBar.setCustomView(mSearchView, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
    }

    private void setupTabs(){
        tabHost.setup();
        addTab(tagFav, R.string.my_conferences, R.drawable.ic_sched, R.id.favs);
        addTab(tagSearch, R.string.search, R.drawable.ic_search_white_24dp, R.id.search);
        tabHost.setOnTabChangedListener(this);
    }

    private void addTab(String tag, @StringRes int title, @DrawableRes int indicator, @IdRes int resourceId){
        View v = getLayoutInflater().inflate(R.layout.tabitemmodel, null);
        TintableImageView image = (TintableImageView) v.findViewById(R.id.tabImage);
        image.setImageResource(indicator);
        image.setColorFilter(getResources().getColorStateList(R.color.image_tint));
        TextView textView = (TextView)v.findViewById(R.id.tabTitle);
        textView.setTextColor(getResources().getColorStateList(R.color.text_tint));
        textView.setText(title);
        tabHost.addTab(tabHost.newTabSpec(tag).setIndicator(v).setContent(resourceId));
    }

    private void loadFavorites(final boolean doOnline){
        updating = true;
        showProgressBar(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                favoritesAdapter = controller.getFavorites();
                if(favoritesAdapter!= null && favoritesAdapter.getCount()>0)
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            favorites.setVisibility(View.VISIBLE);
                        }
                    });
                reloadFavs();
                updating = false;
                if(doOnline)
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            favoritesAdapter = controller.getFavoritesOnline();
                            reloadFavs();
                        } catch (NetworkErrorException e){
                            showToast();
                        }
                        if (refreshLayout.isRefreshing())
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                refreshLayout.setRefreshing(false);
                            }
                        });

                    }
                }).start();
            }
        }).start();
    }

    private void showToast(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, R.string.no_network_toast, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void reloadFavs(){
        Log.d(TAG, "reloadFavs() : RELOADING FAVS " + favoritesAdapter.getCount());
        updateAdapterList(favorites, favoritesAdapter);
    }

    private void loadFirst(){
        showProgressBar(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ConferenceListAdapter adapter = controller.firstConferences();
                    updateAdapterList(search, adapter);
                } catch (NetworkErrorException e){
                    showMessage(R.string.no_conf);
                    showToast();
                }
            }
        }).start();
    }

    private void resetSearch(){
        search.setAdapter(null);
        loadFirst();
    }

    private void doSearch(final CharSequence query){
        showProgressBar(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ConferenceListAdapter adapter = controller.searchConferences(query);
                    updateAdapterList(search, adapter);
                } catch (NetworkErrorException e){
                    showMessage(R.string.no_conf);
                    Toast.makeText(MainActivity.this, R.string.no_network_toast, Toast.LENGTH_LONG).show();
                }
            }
        }).start();
    }

    private void updateAdapterList(final ListView view, final ConferenceListAdapter adapter) {
        showProgressBar(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (refreshLayout.isRefreshing())
                    refreshLayout.setRefreshing(false);
                view.setAdapter(adapter);
                hideMessage();
                if (adapter.isEmpty()) {
                    if (view.equals(search) && tagSearch.equals(currentTag)) {
                        showMessage(R.string.no_conf);
                    } else {
                        showTelaPrincipal(true);
                    }

                } else {
                    if(view.equals(favorites))
                        showTelaPrincipal(false);
                }
            }
        });
    }

    private AdapterView.OnItemClickListener getConferenceClickListener(){
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean isFav = false;
                ParseObject p = (ParseObject) parent.getAdapter().getItem(position);
                isFav = parent != search || Utils.favoriteConferences != null && Utils.favoriteConferences.contains(p);
                Utils.setConference(p);
                Intent intent = new Intent(MainActivity.this, ConferenceActivity.class);
                intent.putExtra("isfav", isFav);
                startActivityForResult(intent, 0);
            }
        };
    }

    private void showTelaPrincipal(final boolean mostrar){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mostrar) {
                    mainTela.setVisibility(View.VISIBLE);
                    favorites.setVisibility(View.GONE);
                } else {
                    mainTela.setVisibility(View.GONE);
                    favorites.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if(tabHost.getCurrentTabTag().equals(tagSearch) && !query.equals("")){
            doSearch(query);
        }
        View v = this.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(v != null)
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if("".equals(newText)){
            if(currentTag.equals(tagSearch))
                resetSearch();
            else
                reloadFavs();
        } else {
            if(currentTag.equals(tagFav)) {
                if(favoritesAdapter!= null && favoritesAdapter.getCount() > 0) {
                    ConferenceListAdapter adapter = controller.filterFavs(newText);
                    updateAdapterList(favorites, adapter);
                } else
                    showTelaPrincipal(true);
            }
        }
        return true;
    }

    @Override
    public void onTabChanged(String tabId) {
        currentTag = tabId;
        hideMessage();
        if(tabId.equals(tagFav)){
            tabHost.getCurrentView().setAnimation(outToRightAnimation());
            setActionBar(true);
            mSearchView.setQuery("", false);
            if(favoritesAdapter == null) {
                if (updating) {
                    showProgressBar(true);
                }
            } else {
                mSearchView.setQuery("", false);
            }
        } else {
            tabHost.getCurrentView().setAnimation(inFromRightAnimation());
            setActionBar(false);
            resetSearch();
        }
    }

    @Override
    public void onRefresh() {
        loadFavorites(true);
        if(currentTag.equals(tagSearch)) {
            String st = mSearchView.getQuery().toString();
            mSearchView.setQuery(st, true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        loadFavorites(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("pedingIntents", Utils.alarms);
    }

    private void showProgressBar(final boolean visible){
        hideMessage();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(visible) {
                    pBar.setVisibility(View.VISIBLE);
                } else {
                    pBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showMessage(@StringRes final int message){
        showProgressBar(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageTextView.setText(message);
                messageTextView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideMessage(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageTextView.setVisibility(View.GONE);
            }
        });
    }

    public void buscarConf(View view) {
        tabHost.setCurrentTab(1);
    }

    public Animation inFromRightAnimation()
    {
        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(240);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    public Animation outToRightAnimation()
    {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(240);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int topRowVerticalPosition = (view == null || view.getChildCount() == 0) ? 0 : view.getChildAt(0).getTop();
        refreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
    }
}
