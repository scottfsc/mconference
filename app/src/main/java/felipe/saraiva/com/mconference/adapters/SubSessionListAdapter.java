package felipe.saraiva.com.mconference.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.util.Linkify;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

import felipe.saraiva.com.mconference.R;

public class SubSessionListAdapter extends BaseAdapter {

    private Context mContext;
    private List list;
    private LayoutInflater lf;

    /**
     * @param context The context associated to this adapter.
     * @param list    The object list which populate the {@link android.widget.ListView} object.
     */
    public SubSessionListAdapter(Context context, @NonNull List list) {
        this.mContext = context;
        this.list = list;
        lf = LayoutInflater.from(mContext);
    }

    /**
     * @param position used to retrieve data type from the list
     * @return view type associated to the object for the given position
     */
    @Override
    public int getItemViewType(int position) {
        return (list.get(position) instanceof ParseObject) ? 1 : 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SessionHolder holder = null;
        Object ob = getItem(position);
        //Se convertView == null, cria nova view dependendo do tipo do dado
        if (convertView == null) {
            if (getItemViewType(position) == 1) { //getItemViewType() retorna 1 quando for do tipo ParseObject
                holder = new SessionHolder();
                convertView = lf.inflate(R.layout.subsessionitemmodel, null);
                holder.title = (TextView) convertView.findViewById(R.id.subsessionTitle);
                holder.authors = (TextView) convertView.findViewById(R.id.subsessionAuthors);
                holder.content = (TextView) convertView.findViewById(R.id.subsessionText);
                convertView.setTag(holder);
            } else {
                convertView = new TextView(mContext);
            }
        } else {
            //Se convertView não for nulo, passa para holder o objeto q armazena os endereços dos TextViews associados
            holder = (SessionHolder) convertView.getTag();
        }
        //holder == null quando o objeto é do tipo String, ou seja, um título p/ as subsessões
        if (holder != null) {
            holder = (SessionHolder) convertView.getTag();
            holder.title.setText(((ParseObject) ob).getString("title"));
            String s1 = ((ParseObject) ob).getString("authors");
            if (s1 != null && s1.length() > 0) {
                holder.authors.setText(s1);
            } else
                holder.authors.setVisibility(View.GONE);
            String s = ((ParseObject) ob).getString("descriptionText");
            if (s != null && s.length() > 0)
                holder.content.setText(s);
            else
                holder.content.setVisibility(View.GONE);
        } else {
            TextView aux = (TextView) convertView;
            aux.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            aux.setTextColor(Color.BLACK);
            aux.setAutoLinkMask(Linkify.WEB_URLS);
            aux.setLinkTextColor(mContext.getResources().getColor(R.color.secundaria));
            aux.setText(ob.toString());
        }

        return convertView;
    }

    static class SessionHolder {
        TextView title;
        TextView authors;
        TextView content;
    }
}
