package felipe.saraiva.com.mconference.work;

/**
 * Created by felip on 26/10/2015.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.BigTextStyle;

import felipe.saraiva.com.mconference.R;
import felipe.saraiva.com.mconference.view.MainActivity;

public class NotifyService extends BroadcastReceiver {

    /**
     * This receiver receives a broadcast intent from Android alarm service
     *
     * @param context The context associated to this BroadcastReceiver
     * @param intent  The intent data passed to this broadcast
     */
    public void onReceive(Context context, Intent intent) {

        String title = intent.getStringExtra("title");
        int i = intent.getIntExtra("message", 0);
        String aux = intent.getStringExtra("ObjectId");
        Utils.alarms.remove(aux);
        String st;
        switch (i) {
            default:
                st = context.getString(R.string.lblsession) + title + "\n" + context.getString(R.string.rightnow);
                break;
            case 1:
                st = context.getString(R.string.lblsession) + title + "\n" + context.getString(R.string.infive);
                break;
            case 2:
                st = context.getString(R.string.lblsession) + title + "\n" + context.getString(R.string.infift);
                break;
            case 3:
                st = context.getString(R.string.lblsession) + title + "\n" + context.getString(R.string.inhalf);
                break;
            case 4:
                st = context.getString(R.string.lblsession) + title + "\n" + context.getString(R.string.inhour);
                break;
            case 5:
                st = context.getString(R.string.lblsession) + title + "\n" + context.getString(R.string.intwoh);
                break;
            case 6:
                st = context.getString(R.string.lblsession) + title + "\n" + context.getString(R.string.inday);
        }
        //Constroi a mensagem que será exibida ao usuário
        String msg = context.getString(R.string.my_app_name) + "\n" + title;
        //Cria uma nova intent para lançar a activity ao clicar na notificação
        Intent notIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notIntent, 0);
        Bitmap localBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.mconference_icon);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentIntent(pendingIntent)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setSmallIcon(R.drawable.ic_push_notify)
                .setContentTitle("mConference")
                .setLargeIcon(localBitmap)
                .setContentText(st)
                .setWhen(System.currentTimeMillis())
                .setStyle(new BigTextStyle().bigText(msg))
                .setAutoCancel(true)
                .setVibrate(new long[]{20L, 500L, 20L, 500L});
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, builder.build());

    }

}
