package felipe.saraiva.com.mconference.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.parse.ParseObject;

import java.util.HashMap;
import java.util.List;

import felipe.saraiva.com.mconference.work.AsyncImageDownloader;

/**
 * Created by felip on 28/10/2015.
 */
public class GalleryGridAdapter extends BaseAdapter {

    private Context mContext;
    private List list;
    private LayoutInflater lf;
    private DisplayMetrics dm = new DisplayMetrics();
    private int imageSize = 0;
    private HashMap<Integer, Bitmap> images = new HashMap<>();

    public GalleryGridAdapter(Context context, List list){
        mContext = context;
        this.list = list;
        lf = LayoutInflater.from(context);
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(imageSize == 0)
            imageSize = (int) ((float)(dm.widthPixels - (20 * dm.densityDpi/160))/((GridView)parent).getNumColumns());
        if(convertView == null){
            convertView = new ImageView(mContext);
            ((ImageView)convertView).setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setLayoutParams(new ViewGroup.LayoutParams(imageSize, imageSize));
        } else {
            ((ImageView) convertView).setImageDrawable(null);
        }
        ParseObject p = (ParseObject) getItem(position);
        convertView.setBackgroundResource(android.R.drawable.stat_notify_error);
        new AsyncImageDownloader((ImageView) convertView).execute(p.getParseFile("thumbnail"));
        return convertView;
    }

}
