package felipe.saraiva.com.mconference.application;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;

import java.io.File;

import felipe.saraiva.com.mconference.R;

/**
 * Created by FelipeFsc on 11/09/2015.
 */
public class mApplication extends Application {


    boolean dev = false;
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("mApplication","App Running");
        File f = new File("data/data/felipe.saraiva.com.mconference2/0");
        System.out.println("O arquivo existe? "+f.exists());
        if(!f.exists()) {
            clearApplicationData();
            try {
                f.createNewFile();
            }catch (Exception e){}
        }
        String appId = getString(dev? R.string.devAppId: R.string.AppId);
        String clientKey = getString(dev? R.string.devClientKey: R.string.ClientKey);
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(appId)
                .clientKey(clientKey)
                .server(getString(R.string.server)).enableLocalDataStore().build());

    }

    private void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

}
