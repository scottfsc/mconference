package felipe.saraiva.com.mconference.work;

import android.app.PendingIntent;
import android.text.format.DateFormat;
import android.util.Log;

import com.parse.ParseObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by felip on 25/10/2015.
 */
public class Utils {

    public static ArrayList<ParseObject> favoriteConferences = null;
    public static HashMap<String, PendingIntent> alarms = null;
    public static TimeZone zn;

    public static ParseObject conference = null;

    public static void setConference(ParseObject conference){
        Utils.conference = conference;
        int t = conference.getInt("timeZoneOffset");
        String sig = t < 0?"-":"+";
        t = Math.abs(t);
        int h = t / 3600;
        int m = (t - (h * 3600))/60;
        String min;
        if(m > 10){
            min = String.valueOf(m);
        } else {
            min = "0"+m;
        }
        String g = "GMT"+sig+h+min;
        zn = TimeZone.getTimeZone(g);
        Log.d("TIMEZONE", zn.getDisplayName());
    }

    public static String getDate(Date date){
        java.text.DateFormat format = DateFormat.getLongDateFormat(null);
        format.setTimeZone(zn);
        return format.format(date);
    }

    public static String getFullDatePlusBRT(Date date){
        return getFullDate(date)+" "+zn.getDisplayName(false, TimeZone.SHORT);
    }

    public static String getFullDate(Date date){
        java.text.DateFormat format = DateFormat.getLongDateFormat(null);
        format.setTimeZone(zn);
        java.text.DateFormat f1 = new SimpleDateFormat("EEEE", Locale.getDefault());
        java.text.DateFormat f2 = new SimpleDateFormat("kk:mm", Locale.getDefault());
        f1.setTimeZone(zn);
        f2.setTimeZone(zn);
        return f1.format(date)+", "+format.format(date)+" "+f2.format(date);
    }

}
