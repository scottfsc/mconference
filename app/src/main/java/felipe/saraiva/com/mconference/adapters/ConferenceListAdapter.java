package felipe.saraiva.com.mconference.adapters;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import felipe.saraiva.com.mconference.R;
import felipe.saraiva.com.mconference.work.AsyncImageDownloader;

/**
 * Created by felip on 25/10/2015.
 */
public class ConferenceListAdapter extends BaseAdapter {

    Context mContext;
    List listData = new ArrayList();
    LayoutInflater layoutInflater;

    public ConferenceListAdapter(Context context, List data){
        mContext = context;
        if(data != null)
            listData = data;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.conferenceitemmodel, null);
            holder.image = (ImageView) convertView.findViewById(R.id.conferenceImage);
            holder.title = (TextView)convertView.findViewById(R.id.conferenceTitle);
            holder.date = (TextView)convertView.findViewById(R.id.conferenceDate);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ParseObject p = (ParseObject) getItem(position);
        new AsyncImageDownloader(holder.image).execute(p.getParseFile("image"));

        holder.title.setText(p.getString("name"));

        StringBuilder st = new StringBuilder();
        st.append(p.getString("city")).append(", ").append(p.getString("state")).append(", ");
        java.text.DateFormat format = DateFormat.getLongDateFormat(mContext);
        TimeZone t = TimeZone.getTimeZone("GMT");
        format.setTimeZone(t);
        int timeOffset = p.getInt("timeZoneOffset");
        long time = p.getDate("startDate").getTime()+ timeOffset*1000;
        st.append(format.format(new Date(time)));

        holder.date.setText(st);

        return convertView;
    }

    static class ViewHolder {
        ImageView image;
        TextView title;
        TextView date;
    }
}
