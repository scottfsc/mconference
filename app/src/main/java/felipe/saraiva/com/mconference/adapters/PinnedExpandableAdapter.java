package felipe.saraiva.com.mconference.adapters;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import felipe.saraiva.com.mconference.R;
import felipe.saraiva.com.mconference.work.Utils;

/**
 * Created by felip on 27/10/2015.
 */
public class PinnedExpandableAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List headers = new ArrayList();
    private SparseArray<ArrayList> map = new SparseArray<>();
    private LayoutInflater lf;

    public PinnedExpandableAdapter(Context context, List headers, SparseArray<ArrayList> objects){
        mContext = context;
        this.headers = headers;
        lf = LayoutInflater.from(context);
        map = (SparseArray<ArrayList>) objects;
    }

    @Override
    public int getGroupCount() {
        return map.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return map.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return map.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return map.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return (groupPosition << 32) & childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            convertView = lf.inflate(R.layout.sublist_textview, null);
            holder = new Holder();
            holder.left = (TextView) convertView.findViewById(R.id.subsectionDate);
            holder.right = (TextView) convertView.findViewById(R.id.subsectionDate2);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        String t[] = ((String)headers.get(groupPosition)).split(";");
        holder.left.setText(t[0]);
        holder.right.setText(t[1]);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            convertView = lf.inflate(R.layout.sessionitemmodel, null);
            holder = new Holder();
            holder.middle = (TextView) convertView.findViewById(R.id.sessiontitleitem);
            holder.left = (TextView) convertView.findViewById(R.id.sessionTime);
            holder.right = (TextView) convertView.findViewById(R.id.sessionPlace);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        ParseObject p = (ParseObject) map.get(groupPosition).get(childPosition);
        holder.middle.setText(p.getString("title"));
        holder.left.setText(getSessionTime(p.getDate("startDate"), p.getDate("endDate")));
        holder.right.setText(p.getString("location"));
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class Holder {
        TextView left;
        TextView middle;
        TextView right;
    }

    public static String getSessionTime(Date date1, Date date2)
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(Utils.zn);
        calendar.setTime(date1);
        StringBuilder builder = new StringBuilder();
        if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            builder.append("0");
        }
        builder.append(calendar.get(Calendar.HOUR_OF_DAY));
        builder.append(":");
        if (calendar.get(Calendar.MINUTE) < 10) {
            builder.append("0");
        }
        builder.append(calendar.get(Calendar.MINUTE));
        builder.append(" - ");
        calendar.setTime(date2);
        if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            builder.append("0");
        }
        builder.append(calendar.get(Calendar.HOUR_OF_DAY));
        builder.append(":");
        if (calendar.get(Calendar.MINUTE) < 10) {
            builder.append("0");
        }
        builder.append(calendar.get(Calendar.MINUTE));
        return builder.toString();
    }
}
