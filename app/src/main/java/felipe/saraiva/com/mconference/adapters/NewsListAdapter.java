package felipe.saraiva.com.mconference.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseObject;
import felipe.saraiva.com.mconference.R;
import felipe.saraiva.com.mconference.work.Utils;

import java.util.List;

/**
 * Created by felip on 28/10/2015.
 */
public class NewsListAdapter extends BaseAdapter {
    private Context mContext;
    private List list;
    private LayoutInflater lf;

    public NewsListAdapter(Context context, List list){
        mContext = context;
        this.list = list;
        lf = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            convertView = lf.inflate(R.layout.newsitemmodel, null);
            holder = new Holder();
            holder.title = (TextView) convertView.findViewById(R.id.newsTitle);
            holder.time = (TextView) convertView.findViewById(R.id.newsDate);
            holder.content = (TextView) convertView.findViewById(R.id.newsContent);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        ParseObject p = (ParseObject) getItem(position);
        holder.title.setText(p.getString("title"));
        holder.content.setText(p.getString("text"));
        holder.time.setText(Utils.getFullDatePlusBRT(p.getUpdatedAt()));
        return convertView;
    }

    static class Holder{
        TextView title;
        TextView time;
        TextView content;
    }
}
