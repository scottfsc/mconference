package felipe.saraiva.com.mconference.work;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.parse.ParseFile;

import java.lang.ref.WeakReference;

/**
 * Created by felip on 25/10/2015.
 */
public class AsyncImageDownloader extends AsyncTask<ParseFile, Void, Bitmap> {

    WeakReference<ImageView> reference;
    private Context mContext;


    public AsyncImageDownloader(ImageView imageView){
        mContext = imageView.getContext();
        reference = new WeakReference<>(imageView);
    }

    @Override
    protected Bitmap doInBackground(ParseFile... params) {
        Bitmap bmp = null;
        try {
            byte[] byteArray = params[0].getData();
            bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        ImageView view = reference.get();
        if(view != null){
            view.setBackground(null);
            if(bitmap != null){
                view.setImageBitmap(bitmap);
            }
        }
    }
}
