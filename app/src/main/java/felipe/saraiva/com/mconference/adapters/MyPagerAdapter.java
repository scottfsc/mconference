package felipe.saraiva.com.mconference.adapters;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

import felipe.saraiva.com.mconference.R;
import felipe.saraiva.com.mconference.application.TouchImageView;
import felipe.saraiva.com.mconference.work.AsyncImageDownloader;

/**
 * Created by felip on 27/10/2015.
 */
public class MyPagerAdapter extends PagerAdapter {

    private List list;
    private LayoutInflater inflater;

    // constructor
    public MyPagerAdapter(Activity activity, List list) {
        this.list = list;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ObjHolder holder = new ObjHolder();
        View viewLayout = inflater.inflate(R.layout.imagegalleryitem, container,false);

        holder.imageView = (TouchImageView) viewLayout.findViewById(R.id.galleryPhoto);
        holder.textView = (TextView) viewLayout.findViewById(R.id.galleryPhotoTitle);

        ParseObject p = (ParseObject) list.get(position);
        new AsyncImageDownloader(holder.imageView).execute(p.getParseFile("thumbnail"));
        new AsyncImageDownloader(holder.imageView).execute(p.getParseFile("imageFile"));
        holder.textView.setText(p.getString("imageName"));
        if(holder.textView.getText() == null || holder.textView.getText().length() == 0)
            holder.textView.setVisibility(View.GONE);
        container.addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    static class ObjHolder{
        TouchImageView imageView;
        TextView textView;
    }
}
