package felipe.saraiva.com.mconference.work;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import felipe.saraiva.com.mconference.adapters.ConferenceListAdapter;
import felipe.saraiva.com.mconference.view.MainActivity;

/**
 * Created by felip on 25/10/2015.
 */
public class MainController {

    MainActivity activity = null;
    private ArrayList<ParseObject> favoritos;

    public MainController(MainActivity activity){
        this.activity = activity;
    }

    public ConferenceListAdapter getFavorites() {
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("UserConference");
        parseQuery.orderByDescending("startDate");
        parseQuery.fromPin("userConference");
        parseQuery.whereEqualTo("user", ParseUser.getCurrentUser());
        parseQuery.include("conference");
        List<ParseObject> list = new ArrayList<>();
        try {
            List<ParseObject> l = parseQuery.find();
            for(ParseObject p: l){
                list.add(p.getParseObject("conference"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utils.favoriteConferences = (ArrayList<ParseObject>) list;
        return new ConferenceListAdapter(activity, list);
    }

    public ConferenceListAdapter getFavoritesOnline() throws NetworkErrorException {
        List<ParseObject> list = new ArrayList<>();
        NetworkInfo nf = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if( nf!= null && nf.isConnected()) {
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("UserConference");
            parseQuery.orderByDescending("startDate");
            parseQuery.whereEqualTo("user", ParseUser.getCurrentUser());
            parseQuery.include("conference");
            try {
                List<ParseObject> l = parseQuery.find();
                ParseObject.pinAllInBackground("userConference",l);
                for (ParseObject p : l) {
                    list.add(p.getParseObject("conference"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ParseObject.unpinAllInBackground("favorites");
            Utils.favoriteConferences = (ArrayList<ParseObject>) list;
            ParseObject.pinAllInBackground("favorites", Utils.favoriteConferences);
            return new ConferenceListAdapter(activity, list);
        }
        throw new NetworkErrorException();
    }

    public ConferenceListAdapter filterFavs(CharSequence filter){
        favoritos = Utils.favoriteConferences;
        String st = "";
        if(filter != null && filter != "") {
            st = filter.toString().toLowerCase();
            ArrayList<ParseObject> aux = new ArrayList<>();
            for (ParseObject p : favoritos) {
                if(p.getString("name").toLowerCase().contains(st))
                    aux.add(p);
            }
            return new ConferenceListAdapter(activity, aux);
        }
        return new ConferenceListAdapter(activity, null);
    }

    public ConferenceListAdapter firstConferences() throws NetworkErrorException {
        NetworkInfo nf = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if( nf!= null && nf.isConnected()) {
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Conference");
            parseQuery.orderByDescending("startDate");
            parseQuery.setLimit(1000);
            List<ParseObject> list = null;
            try {
                list = parseQuery.find();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ConferenceListAdapter(activity, list);
        }
        throw new NetworkErrorException();
    }

    public ConferenceListAdapter searchConferences(CharSequence query) throws NetworkErrorException {
        NetworkInfo nf = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if( nf!= null && nf.isConnected()) {
            String[] st = query.toString().split(" ");
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Conference");
            parseQuery.whereContainedIn("tags", Arrays.asList(st));
            List<ParseObject> list = null;
            try {
                list = parseQuery.find();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ConferenceListAdapter(activity, list);
        }
        throw new NetworkErrorException();
    }
}
