package felipe.saraiva.com.mconference.work;


import android.accounts.NetworkErrorException;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.util.SparseArray;
import android.widget.BaseAdapter;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import felipe.saraiva.com.mconference.R;
import felipe.saraiva.com.mconference.adapters.GalleryGridAdapter;
import felipe.saraiva.com.mconference.adapters.MyPagerAdapter;
import felipe.saraiva.com.mconference.adapters.NewsListAdapter;
import felipe.saraiva.com.mconference.adapters.PinnedExpandableAdapter;
import felipe.saraiva.com.mconference.view.ConferenceActivity;

/**
 * Created by felip on 26/10/2015.
 */
public class ConferenceController {

    private static final String TAG = "contr.ConferenceController";
    ConferenceActivity activity;
    ParseObject conference;
    private List<ParseObject> sessions, favorites;
    private MyPagerAdapter gallery = null;

    public ConferenceController(ConferenceActivity activity){
        this.activity = activity;
        conference = Utils.conference;
        favorites = new ArrayList<>();
    }

    public ParseObject getConference(String conferenceId){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Conference");
        query.whereEqualTo("objectId", conferenceId);
        query.fromPin("favorites");
        try {
            Utils.conference = query.getFirst();
            return Utils.conference;
        } catch (ParseException e) {
            return null;
        }
    }

    public PinnedExpandableAdapter getSessions() {
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("ConferenceSession");
        Log.d("Teste","Numero de máximo: "+parseQuery.getLimit());
        parseQuery.fromPin("sessions");
        parseQuery.whereEqualTo("conference", conference);
        parseQuery.orderByAscending("startDate");
        parseQuery.include("subsessions");
        parseQuery.setLimit(1000);
        List<ParseObject> list;
        try {
            list = parseQuery.find();
        } catch (Exception e) {
            list = new ArrayList<>();
        }
        sessions = list;
        return pinnedAdapterFromList(list);
    }

    public PinnedExpandableAdapter getSessionsOnline() throws NetworkErrorException {
        NetworkInfo nf = ((ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if ((nf != null) && nf.isConnected()) {
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("ConferenceSession");
            parseQuery.whereEqualTo("conference", conference);
            parseQuery.orderByAscending("startDate");
            parseQuery.include("subsessions");
            parseQuery.setLimit(1000);
            List<ParseObject> list;
            try {
                list = parseQuery.find();
                ParseObject.unpinAll("sessions");
                ParseObject.pinAllInBackground("sessions", list);
                sessions = list;
                return pinnedAdapterFromList(list);
            } catch (Exception e) {
            }
        }
        throw new NetworkErrorException();
    }

    public PinnedExpandableAdapter getFavorites(ParseObject userConference) {
        List<ParseObject> list;
        try {
            list = userConference.getList("favoriteSessions");
            ordenar(list);
            favorites = list;
        } catch (NullPointerException e){
            list = new ArrayList<>();
        }
        return pinnedAdapterFromList(list);
    }

    private void ordenar(List list){
        Comparator<ParseObject> c = new Comparator<ParseObject>() {
            @Override
            public int compare(ParseObject lhs, ParseObject rhs) {
                return lhs.getDate("startDate").compareTo(rhs.getDate("startDate"));
            }
        };
        Collections.sort(list, c);
    }

    public PinnedExpandableAdapter filtrarSessoes(String query, boolean favoritas){
        ArrayList<ParseObject> aux = new ArrayList<>();
        List<ParseObject> l = (favoritas)? favorites:sessions;
        for(ParseObject p: l){
            if (p.getString("title").toLowerCase().contains(query.toLowerCase())) {
                aux.add(p);
            }
        }
        return pinnedAdapterFromList(aux);
    }

    public MyPagerAdapter getLocationPhotos(){
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("ConferencePhoto");
        parseQuery.fromPin("location");
        parseQuery.whereEqualTo("conference", conference);
        parseQuery.whereEqualTo("locationPhotos", true);
        List<ParseObject> list;
        try{
             list = parseQuery.find();
        }
        catch (Exception e){
            list = new ArrayList<>();
        }
        return new MyPagerAdapter(activity, list);
    }

    public boolean isFav(ParseObject session){
        if(favorites == null)
            favorites = new ArrayList<>();
        return favorites.contains(session);
    }

    public MyPagerAdapter getLocationPhotosOnline() throws NetworkErrorException {
        NetworkInfo nf = ((ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if ((nf != null) && nf.isConnected()) {
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("ConferencePhoto");
            parseQuery.whereEqualTo("conference", conference);
            parseQuery.whereEqualTo("locationPhotos", true);
            List list;
            try {
                list = parseQuery.find();
                ParseObject.unpinAll("location");
                ParseObject.pinAllInBackground("location",list);
                return new MyPagerAdapter(activity, list);
            } catch (Exception e) {
            }
        }
        throw new NetworkErrorException();
    }

    public BaseAdapter getNews(){
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("News");
        parseQuery.fromPin("news");
        parseQuery.whereEqualTo("conference", this.conference);
        parseQuery.orderByDescending("updatedAt");
        List list;
        try{
            list = parseQuery.find();
        }
        catch (ParseException localParseException) {
            list = new ArrayList();
        }
        return new NewsListAdapter(activity, list);
    }

    public BaseAdapter getNewsOnline() throws NetworkErrorException {
        NetworkInfo nf = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if ((nf != null) && nf.isConnected()) {
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("News");
            parseQuery.whereEqualTo("conference", this.conference);
            parseQuery.orderByDescending("updatedAt");
            List list;
            try {
                list = parseQuery.find();
                ParseObject.unpinAll("news");
                ParseObject.pinAllInBackground("news",list);
                return new NewsListAdapter(activity, list);
            } catch (ParseException localParseException) {
            }
        }
        throw new NetworkErrorException();
    }

    public BaseAdapter getPhotos(){
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("ConferencePhoto");
        parseQuery.fromPin("photos");
        parseQuery.whereEqualTo("conference", conference);
        parseQuery.whereNotEqualTo("locationPhotos", true);
        parseQuery.orderByDescending("updatedAt");
        List list;
        try{
            list = parseQuery.find();
        } catch (ParseException e){
            list = new ArrayList();
        }
        gallery = new MyPagerAdapter(activity, list);
        return new GalleryGridAdapter(activity, list);
    }

    public BaseAdapter getPhotosOnline() throws NetworkErrorException   {
        NetworkInfo nf = ((ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if ((nf != null) && nf.isConnected()) {
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("ConferencePhoto");
            parseQuery.whereEqualTo("conference", conference);
            parseQuery.whereNotEqualTo("locationPhotos", true);
            parseQuery.orderByDescending("updatedAt");
            List list;
            try {
                list = parseQuery.find();
                ParseObject.unpinAll("photos");
                ParseObject.pinAllInBackground("photos",list);
            } catch (ParseException e) {
                throw new NetworkErrorException();
            }
            gallery = new MyPagerAdapter(activity, list);
            Log.d("control.Conference", "getPhotosOnline() gallery count: " + gallery.getCount());
            return new GalleryGridAdapter(activity, list);
        }
        throw new NetworkErrorException();
    }

    public MyPagerAdapter getGalleryPageAdapter(){
        if(gallery != null)
            return gallery;
        return new MyPagerAdapter(activity, new ArrayList());
    }

    @SuppressWarnings("unchecked")
    public PinnedExpandableAdapter pinnedAdapterFromList(List<ParseObject> list){
        if (list == null) {
            return new PinnedExpandableAdapter(activity, new ArrayList(), new SparseArray<ArrayList>());
        }
        ArrayList<String> aux = new ArrayList<>();
        SparseArray<ArrayList> map = new SparseArray<>();
        int i = 0;
        String st2 = null;
        for (ParseObject p : list) {
            String str = Utils.getDate(p.getDate("startDate"));
            if (!str.equals(st2)) {
                map.put(i, new ArrayList<>());
                StringBuilder st = new StringBuilder().append(str).append(";").append(this.activity.getResources().getString(R.string.day)).append(" ");
                st.append(++i);
                aux.add(st.toString());
                st2 = str;
            }
            map.get(i-1).add(p);
        }
        return new PinnedExpandableAdapter(this.activity, aux, map);
    }
}
